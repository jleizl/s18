// Function able to receive data without the use of global variables or prompt()

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
function printName(name){
	console.log("My name is " + name);
};

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Jungkook");
printName("Thonie");

// Data passed into the function through funtion invocation is called arguments
// The argument is then stored within a container called a parameter
function printMyAge(age){
	console.log("I am " + age);
};

printMyAge(25);
printMyAge();

// check divisibility reusably using a function with arguments and parameter
function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
};
checkDivisibilityBy8(64);
checkDivisibilityBy8(27);
/*
	Mini-Activity

	1. Create a function which is capable to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console
	 2. Create a function which is capable to receive a number as an argument:
	 	- This function should be able to receive a any number
	 	- Display the number and state if even number
 */


function getFavoriteSuperhero(superhero){
	console.log(superhero);
}

getFavoriteSuperhero("Iron Man");


function inputNumber(number){
	let remainder = number % 2;
	let isEvenNumber = remainder === 0;

	console.log(number + " is an even number.");
	console.log(isEvenNumber)
};
inputNumber(50);
inputNumber(33);


// Multiple Arguments can also be passed into a function; multiple parameters can contain our arguments

function printFullName(firstName, middleInitial, lastName){
	console.log(firstName + ' ' + middleInitial +' ' + lastName);
};

printFullName('Juan', 'Crisostomo', 'Ibarra');
printFullName('Ibarra', 'Juan', 'Crisostomo');

/*
	Parameters will contain the argument according to the order it was passed

	In other language, providing more/less arguments than the expected parameters sometime causes an errot or changes the behavior of the function

 */

printFullName('Stephen', 'Wardell');
printFullName('Stephen', 'Wardell', 'Curry', 'Thomas');
printFullName('Stephen', 'Wardell', 'Curry');
printFullName('Stephen', ' ', 'Curry');

// Use variables as arguments
let fName = "Larry";
let mName = "Joe";
let lName = "Bird";

printFullName(fName, mName, lName);

/* Mini - Activity
	Create a function which will be able to receive 5 arguments
		- Receive 5 of your favorite songs
		- Display/print the passed 5 favorite songs in the console when the function is invoked.

*/

function print5FavoriteSongs(song1, song2, song3, song4, song5){
	console.log("My top 5 favorite songs.");
	console.log(song1);
	console.log(song2);
	console.log(song3);
	console.log(song4);
	console.log(song5);

	// return song1 + song2 + song3 + song4 + song5;
	// return "Hello, Sir Rupert";
};

print5FavoriteSongs("Rainbow", "When I met you", "Tahanan", "Muli", "Beautiful in my eyes");

let fullName = printFullName("Gabriele", "Vhernadette", "Fernandez");
	console.log(fullName);

function returnFullName(firstName, middleName, lastName){
	return firstName + ' ' + middleName + ' ' + lastName;
};

fullName = returnFullName("Juan", "Ponce", "Enrile");
console.log(fullName);
console.log(fullName + "is my grandpa.");


/**/

function returnPhilippineAddress(city){
	return city + " , Philippines.";
};

let myFullAddress = returnPhilippineAddress("San Pedro");
console.log(myFullAddress);


function checkDivisibilityBy4(number){
	let remainder = number % 4;
	let isDivisibleBy4 = remainder === 0;
	return isDivisibleBy4;
	console.log("I am run after the return.");
};

let num4isDivisibleBy4 = checkDivisibilityBy4(4);
let num14isDivisibleBy4 = checkDivisibilityBy4(14);

checkDivisibilityBy4(4);
console.log(num4isDivisibleBy4);
console.log(num14isDivisibleBy4);




function createPlayerInfo(username, level, job){
	// console.log("username: " + username + "level " + level + "Job: " + job);
	return("username: " + username + "level " + level + "Job: " + job);
	console.log(1+1);
};

let user1 = createPlayerInfo("white night", 95, "Paladin");
console.log(user1);


// Mini-Activity


function addTwoNumbers(number1, number2){
		return (number1 + number2);
}

let sum= addTwoNumbers(10, 15);
console.log(sum);


// Example

function productOfTwoNumbers(num1,num2){
	return(num1 * num2);
};

let product = productOfTwoNumbers(55, 60);
console.log(product);


/*
  Create a function which wikk be able to get the area of a circle from a provided radius
     - a number should be provided as an argument
     - look up for the formula for calculating the area of a circle 
     - look up for the use of exponent operator
     - you can save the value of the calculations in a variable
     - return the result of the area 

   Create a global variable called outside of the function called circleArea
    - this variable should be able to receive and store the result of the calculation of area of a circle
   Log the value of the circleArea in the console.

 */

function getAreaOfCircle(radius){
	return(3.14 * (radius ** 2));
};

	let circleArea = getAreaOfCircle(20);
	console.log(circleArea);



/*
Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */


//  Answer to assignment
function checkScorePassed(score, totalscore){
	let scorePercentage = (score / totalscore) * 100;
	let passingPercentage = 75;
	let isPassed = scorePercentage >= passingPercentage;
	return isPassed;
};

let isPassingScore = checkScorePassed(90,100);
console.log(isPassingScore);
